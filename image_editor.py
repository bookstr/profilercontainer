from skimage import io, exposure
from skimage import img_as_ubyte
from skimage.color import rgb2gray, rgba2rgb

class ImageEditor:
    @staticmethod
    def brighten_image(source_filename, target_filename):
        image = io.imread(source_filename)
        brightened_image = exposure.adjust_gamma(image, 0.1) 
        io.imsave(fn=target_filename, arr=img_as_ubyte(brightened_image))

    @staticmethod
    def monochrome(source_filename, target_filename):
        image = io.imread(source_filename)
        image_grey = rgb2grey(rgba2rgb(image))
        io.imsave(fnname=target_filename, arr=img_as_ubyte(image_grey))
