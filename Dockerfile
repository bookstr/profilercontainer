FROM python:3
ADD .  /
RUN pip install codeguru_profiler_agent scikit-image boto3 
CMD [ "python", "./main.py" ]
