## Profiler Container 
[This is the repo that the container is based on](https://github.com/aws-samples/aws-codeguru-profiler-python-demo-application)

## About the container 
Base - busybox 


### Environment Variables:
The environment variables are in the __env.list__ file 
* DEMO_APP_BUCKET_NAME 
* DEMO_APP_SQS_URL
* REGION=us-east-1
* ACCESS_KEY=
* SECRET_KEY=''
* #PROFILE=coda
* AWS_CONFIG_FILE

### running the container 
```
 docker run --env-file env.list profiler:1
```

## Authenticating with AWS 
there are 2 files which need to authenticate with the boto3 client / session 
* image_processor.py
* task_publisher.py

It should be able to use either a profile name or Access key / Secret access key
(the origional code just used profiles from the command line) 

### Main error I'm currently seeing when I run 
This is the error when using access key / secret access key 
```
Extracting tasks from sqs queue - 'https://sqs.us-east-1.amazonaws.com/182968331794/demoprofiler'
Failed to extract task from sqs queue - 'https://sqs.us-east-1.amazonaws.com/182968331794/demoprofiler'
Failed to process message from SQS queue...
Unable to locate credentials
```

This is the error when using a profile 
```
Traceback (most recent call last):
  File "//./main.py", line 58, in <module>
    SampleDemoApp().run()
  File "//./main.py", line 24, in __init__
    self.task_publisher = TaskPublisher(self.sqs_queue_url, self.s3_bucket_name)
  File "/task_publisher.py", line 38, in __init__
    session = boto3.Session(profile_name=awsprofile)
  File "/usr/local/lib/python3.9/site-packages/boto3/session.py", line 80, in __init__
    self._setup_loader()
  File "/usr/local/lib/python3.9/site-packages/boto3/session.py", line 120, in _setup_loader
    self._loader = self._session.get_component('data_loader')
  File "/usr/local/lib/python3.9/site-packages/botocore/session.py", line 685, in get_component
    return self._components.get_component(name)
  File "/usr/local/lib/python3.9/site-packages/botocore/session.py", line 924, in get_component
    self._components[name] = factory()
  File "/usr/local/lib/python3.9/site-packages/botocore/session.py", line 158, in <lambda>
    lambda:  create_loader(self.get_config_variable('data_path')))
  File "/usr/local/lib/python3.9/site-packages/botocore/session.py", line 240, in get_config_variable
    return self.get_component('config_store').get_config_variable(
  File "/usr/local/lib/python3.9/site-packages/botocore/configprovider.py", line 301, in get_config_variable
    return provider.provide()
  File "/usr/local/lib/python3.9/site-packages/botocore/configprovider.py", line 398, in provide
    value = provider.provide()
  File "/usr/local/lib/python3.9/site-packages/botocore/configprovider.py", line 459, in provide
    scoped_config = self._session.get_scoped_config()
  File "/usr/local/lib/python3.9/site-packages/botocore/session.py", line 340, in get_scoped_config
    raise ProfileNotFound(profile=profile_name)
botocore.exceptions.ProfileNotFound: The config profile (coda_abook) could not be found
```

